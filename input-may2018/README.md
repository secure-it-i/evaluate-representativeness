Each subfolder folder contains the following files pertaining to May 2018 evaluation in Rekha:

1. **common-ghera-benign-id-api-mapping.csv** lists the relevant apis common to apps that were flagged as vulnerable and apps that were not flagged as vulnerable.
2. **detected-only-ghera-benign-id-api-mapping.csv** lists the relevant apis unique to apps that were flagged as vulnerable.
3. **undetected-only-ghera-benign-id-api-mapping.csv** lists the relevant apis unique to apps that were not flagged as vulnerable.
