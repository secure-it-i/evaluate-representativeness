# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause "New" and "Revised" License
#
# Author: Venkatesh-Prasad Ranganath


library(docopt)
library(readr)
library(ggplot2)

print("Loading data")
valid_min_sdks <- as.character(14:27)
valid_target_sdks <- as.character(19:27)
trim_based_on_minSDK_and_targetSDK <- function (data, pos) {
  subset(data, MinSDK %in% valid_min_sdks & TargetSDK %in% valid_target_sdks)
}

args <- docopt(
  "Usage: plot_api_usage.r <ghera_api_profile_csv> <remapped_andro_zoo_api_profile_csv> <suffix> <output_folder> [<api_ids>]")
ghera_data <- readr::read_csv_chunked(
  args[['<ghera_api_profile_csv>']],
  readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
  progress=T)
androzoo_data <- readr::read_csv_chunked(
  args[['<remapped_andro_zoo_api_profile_csv>']],
  readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
  progress=T)
suffix <- args[['<suffix>']]
output_folder <- args[['<output_folder>']]
api_ids_file <- args[['<api_ids>']]

# ghera_data <- readr::read_csv_chunked(
#   "output-top-200/ghera-benign-api-profile.csv",
#   readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
#   progress=T)
# androzoo_data <- readr::read_csv_chunked(
#   "output-top-200/real-world-benign-api-profile.csv",
#   readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
#   progress=T)
# suffix <- "benign"
# api_ids_file <- "output-top-200/security/ghera-benign-id-api-mapping.csv"

if (!is.null(api_ids_file)) {
  api_ids <- as.character(read.csv(api_ids_file, header=F, sep="!")$V1)
  col_names <- colnames(ghera_data)
  tmp1 <- col_names[-1:-3] %in% api_ids 
  tmp2 <- c(col_names[1:3], col_names[-1:-3][tmp1])
  ghera_data <- ghera_data[tmp2]
  androzoo_data <- androzoo_data[tmp2]
}

ghera_data <- ghera_data[!is.na(ghera_data$MinSDK) &
                           !is.na(ghera_data$TargetSDK) &
                           !is.na(ghera_data$APKs),]
androzoo_data <- androzoo_data[!is.na(androzoo_data$MinSDK) &
                                 !is.na(androzoo_data$TargetSDK) &
                                 !is.na(androzoo_data$APKs),]
androzoo_data_nrow <- nrow(androzoo_data)
print(paste("Considering", nrow(androzoo_data), "APKs"))
print(paste("Considering", ncol(androzoo_data), "APIs"))

print("Plotting API usage graph")
target_sdks <- c(19, 21, 22, 23, 24, 25)

ghera_api_proportions <- as.vector(apply(ghera_data[, -1:-3], 2, sum)
                                   / nrow(ghera_data) * 100)
androzoo_api_proportions <- sapply(target_sdks, function(target_sdk) {
  data <- androzoo_data[androzoo_data$TargetSDK >= target_sdk &
                          androzoo_data$MinSDK <= target_sdk,]
  as.vector(apply(data[, -1:-3], 2, sum) / nrow(data) * 100)
}, simplify=F, USE.NAMES=T)
names(androzoo_api_proportions) <- target_sdks
androzoo_api_proportions$all <- as.vector(
  apply(androzoo_data[, -1:-3], 2, sum) / androzoo_data_nrow * 100)

ord <- order(androzoo_api_proportions$`25`, decreasing=T)

png(paste(output_folder, "/api-usage-", suffix, ".png", sep=""), width=800, height=400)
ggplot(NULL, aes(1:length(ord))) +
  #geom_line(aes(y = ghera_api_proportions[ord], linetype = "Ghera")) +
  geom_line(aes(y = androzoo_api_proportions$`19`[ord],
                colour = "API Level 19")) +
  geom_line(aes(y = androzoo_api_proportions$`21`[ord],
                colour = "API Level 21")) +
  geom_line(aes(y = androzoo_api_proportions$`22`[ord],
                colour = "API Level 22")) +
  geom_line(aes(y = androzoo_api_proportions$`23`[ord],
                colour = "API Level 23")) +
  geom_line(aes(y = androzoo_api_proportions$`24`[ord],
                colour = "API Level 24")) +
  geom_line(aes(y = androzoo_api_proportions$`25`[ord],
                colour = "API Level 25")) +
  xlab("APIs in decreasing order of proportions in API Level 25") +
  ylab("Percentage of Apps Using an API") +
  labs(color="API Levels") +
  theme(legend.title=element_text(size=14),
    legend.text=element_text(size=14),
    axis.title.x=element_text(size=14),
    axis.title.y=element_text(size=14),
    axis.text.x=element_text(size=12),
    axis.text.y=element_text(size=12))
dev.off()
