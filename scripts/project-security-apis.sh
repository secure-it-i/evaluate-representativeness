#!/usr/bin/env bash

if [ "$#" -eq 6 ] ; then
  if [ $1 == "-s" ] && [ $3 == "-t" ] && [ $5 == "-o" ]; then
    echo "continue"
    filename="$2"
    head -n 1 $4 > $6
    while read -r line
    do
      id=`echo $line | cut -d'!' -f1`
      outline=`grep ^$id, $4`
      echo $outline >> $6
    done < "$filename"
  else
    echo "Error : Only options : -s -t -o allowed"
  fi
else
  echo "Error : Required args missing : -s <id-mapping-file-path> -t <repr file all path> -o <output file path> in that order"
fi
