# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause "New" and "Revised" License
#
# Author: Venkatesh-Prasad Ranganath


library(docopt)

P_THRESHOLD <- 0.05

args <- docopt(
  "Usage: tabulate_relevance.r <folder> <benign repr regex> <malicious repr regex> <secure repr regex>")

process_files_matching_in <- function(regex, folder) {
  files <- list.files(folder, regex)
  frame <- as.data.frame(t(as.data.frame(sapply(files, function (filename) {
    entries <- unlist(strsplit(gsub(".csv", "", filename), "-"))[c(-1, -3)]
    sample_size <- as.numeric(gsub("n_", "", entries[1]))
    population_size <- 0
    api_level <- "14+"
    if (length(entries) == 3) {
      population_size <- as.numeric(gsub("N_", "", entries[3]))
      api_level <- gsub("API_", "", entries[2])
    } else if (length(entries) == 4) {
      population_size <- as.numeric(gsub("N_", "", entries[4]))
    }
    data <- read.csv(paste(folder, filename, sep="/"), header=T)
    num_features <- nrow(data)
    tmp1 <- sum(data$p_value_g >= P_THRESHOLD)
    relevance <- paste(tmp1, " (", as.integer( tmp1 / num_features * 100),
                       ")", sep="")
    tmp2 <- sum(!is.na(data$power_o) & data$p_value_g >= P_THRESHOLD & 
                data$power_o >= 0.8)
    relevance_with_power <- paste(tmp2, " (",
                                  as.integer(tmp2 / num_features * 100),
                                  ")", sep="")
    tmp3 <- sum(data$prevalent_in_wild)
    prevalent_in_wild <- paste(tmp3, " (", as.integer( tmp3 / num_features *
                                        100), ")", sep="")
    c("API Level"=api_level,
      "Pop Size"=population_size,
      "Sample Size"=sample_size,
      "# of APIs"=num_features,
      "Statistical Relevance (%)"=relevance,
      "Statistical Relevance w/ Power>0.8 (%)"=relevance_with_power,
      "Absolute Relevance (%)"=prevalent_in_wild)
  }))))
  rownames(frame) <- seq(1:nrow(frame))
  frame
}

folder <- args[['<folder>']]
benign_repr_regex <- args[['<benign repr regex>']]
malicious_repr_regex <- args[['<malicious repr regex>']]
secure_repr_regex <- args[['<secure repr regex>']]

tmp1 <- process_files_matching_in(benign_repr_regex, folder)
tmp2 <- process_files_matching_in(malicious_repr_regex, folder)
tmp2[, 1:3] <- NULL
tmp3 <- process_files_matching_in(secure_repr_regex, folder)
tmp3[, 1:3] <- NULL
tmp4 <- cbind(tmp1, tmp2, tmp3)
write.csv(tmp4, "", row.names=F, quote=F)
