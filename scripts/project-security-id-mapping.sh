#!/usr/bin/env bash

if [ "$#" -eq 6 ] ; then
  if [ $1 == "-i" ] && [ $3 == "-s" ] && [ $5 == "-t" ]; then
    echo "continue"
    filename="$2"
    while read -r line
    do
      grep "$line" $4 >> tmp.csv
    done < "$filename"
    sort -n tmp.csv | uniq > $6
    rm tmp.csv
  else
    echo "Error : Only options : -i -s -t allowed"
  fi
else
  echo "Error : Required args missing : -i <security-apis-file-path> -s <source-id-mapping> -t <target-id-mapping> in that order"
fi
