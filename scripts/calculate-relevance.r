# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause "New" and "Revised" License
#
# Author: Venkatesh-Prasad Ranganath
#
#
# Test the relevance of Ghera benchmarks
#
# For each Android API feature X used in a Ghera app,
#   1. Using a subset of AndroZoo data set with minSDK >= 14 and
#      targetSDK >= 19, for sample size of n=40
#   1.1 Calculate the sampling proportion X_Gp of Ghera apps that use X by
#       considering random n-sized samples with replacement.
#   1.2 Calculate the sampling proportion X_Ap from random n-sized samples
#       with replacement.
#   1.3 Compare X_Gp and X_Ap under confidence interval: 0.95
#         Test_t: NH: X_Gp == X_Ap  AH: X_Gp != X_Ap
#         Test_g: NH: X_Gp <= X_Ap  AH: X_Gp > X_Ap
#         Test_l: NH: X_Gp >= X_Ap  AH: X_Gp < X_Ap
#   2. Repeat step 1 with n=40 for following subsets of AndroZoo APKs that
#      can run on API Levels: 19, 21, 22, 23, 24, 25.
#      That is, for API level K, 14 <= app.minSDK <= K <= app.targetSDK
#

library(docopt)
library(readr)
library(parallel)

SAMPLE_SIZE <- 40
P_THRESHOLD <- 0.01

args <- docopt(
  "Usage: calculate_relevance.r <ghera_api_profile_csv> <andro_zoo_api_profile_csv> <prefix>")

print("Loading data")
valid_min_sdks <- as.character(14:27)
valid_target_sdks <- as.character(19:27)
trim_based_on_minSDK_and_targetSDK <- function (data, pos) {
  subset(data, MinSDK %in% valid_min_sdks & TargetSDK %in% valid_target_sdks)
}
ghera_data <- read.csv(args[['<ghera_api_profile_csv>']], header=T,
                       check.names=F)
androzoo_data <- readr::read_csv_chunked(
      args[['<andro_zoo_api_profile_csv>']],
      readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
      progress=T)
prefix <- args[['<prefix>']]
# ghera_data <- read.csv("output/ghera-benign-api-profile.csv", header=T,
#                        check.names=F)
# tmp1 <- readr::read_csv_chunked("output/andro-zoo-benign-api-profile.csv",
#                                 readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
#                                 progress=T)
# prefix <- "output/relevance"

androzoo_data <- androzoo_data[!is.na(androzoo_data$MinSDK) &
                               !is.na(androzoo_data$TargetSDK) &
                               !is.na(androzoo_data$APKs),]
androzoo_data_nrow <- nrow(androzoo_data)
print(paste("Considering", nrow(androzoo_data), "APKs"))

print("Calculating proportion for ghera APKs")
ghera_pop_size <- nrow(ghera_data)

get_num_of_samples <- function(data_size) {
  max(40, as.integer(data_size * .8 / SAMPLE_SIZE))
}

do_prop_test <- function(api_col, data) {
  size <- get_num_of_samples(nrow(data)) * SAMPLE_SIZE
  tmp1 <- sample(1:nrow(data), size=size, replace=T)
  samples_indices <- t(matrix(tmp1, SAMPLE_SIZE))
  apps_props <- apply(samples_indices, 1, function(sample_indices) {
    sum(data[sample_indices, api_col] > 0)
  })
  apps_sampling_prop_mean <- mean(apps_props)
  apps_sampling_prop_sd <- sd(apps_props)

  tmp1 <- sample(1:ghera_pop_size, size=size, replace=T)
  samples_indices <- t(matrix(tmp1, SAMPLE_SIZE))
  ghera_props <- apply(samples_indices, 1, function(sample_indices) {
    sum(ghera_data[sample_indices, api_col] > 0)
  })
  ghera_sampling_prop_mean <- mean(ghera_props)
  ghera_sampling_prop_sd <- sd(ghera_props)

  tmp2 <- matrix(c(ghera_sampling_prop_mean, apps_sampling_prop_mean,
                   SAMPLE_SIZE - ghera_sampling_prop_mean + 0.1,
                   SAMPLE_SIZE - apps_sampling_prop_mean + 0.1), 2)
  result_t <- prop.test(tmp2, alternative="t")
  result_g <- prop.test(tmp2, alternative="g")
  result_l <- prop.test(tmp2, alternative="l")
  ghera_sampling_prop_mean_as_percent =
    ghera_sampling_prop_mean / SAMPLE_SIZE * 100
  apps_sampling_prop_mean_as_percent =
    apps_sampling_prop_mean / SAMPLE_SIZE * 100
  power_t <- power.prop.test(n=SAMPLE_SIZE,
                             ghera_sampling_prop_mean_as_percent / 100,
                             apps_sampling_prop_mean_as_percent / 100,
                             alternative="t")
  power_o <- power.prop.test(n=SAMPLE_SIZE,
                             ghera_sampling_prop_mean_as_percent / 100,
                             apps_sampling_prop_mean_as_percent / 100,
                             alternative="o")
  c(ghera_sampling_prop_mean=ghera_sampling_prop_mean,
    ghera_sampling_prop_sd=ghera_sampling_prop_sd,
    apps_sampling_prop_mean=apps_sampling_prop_mean,
    apps_sampling_prop_sd=apps_sampling_prop_sd,
    ghera_sampling_prop_mean_as_percent=ghera_sampling_prop_mean_as_percent,
    apps_sampling_prop_mean_as_percent=apps_sampling_prop_mean_as_percent,
    p_value_t=result_t$p.value,
    gp_possibly_similar_ap=ifelse(result_t$p.value > P_THRESHOLD, 'T', 'F'),
    p_value_g=result_g$p.value,
    gp_possiby_le_ap=ifelse(result_g$p.value > P_THRESHOLD, 'T', 'F'),
    p_value_l=result_l$p.value,
    gp_possibly_ge_ap=ifelse(result_l$p.value > P_THRESHOLD, 'T', 'F'),
    prevalent_in_wild=
      ifelse(ghera_sampling_prop_mean_as_percent <=
             apps_sampling_prop_mean_as_percent, 'T', 'F'),
    power_t=power_t$power,
    power_o=power_o$power)
}

num_api_cols <- ncol(ghera_data) - 3
api_cols <- seq(1, num_api_cols) + 3

#Step 2
print("Performing proportion test considering all API levels")
num_of_samples <- get_num_of_samples(androzoo_data_nrow)
print(paste(date(), "Number of Samples", num_of_samples,
            "Sample Size", SAMPLE_SIZE,
            "MinSDK >= 14 and TargetSDK >= 19", "Population Size",
            androzoo_data_nrow))
tmp1 <- t(as.data.frame(sapply(api_cols, function (api_col) {
  do_prop_test(api_col, androzoo_data)
})))
rownames(tmp1) <- colnames(ghera_data)[api_cols]
write.csv(tmp1,
          paste(prefix, "-n_", SAMPLE_SIZE, "-samples_", num_of_samples,
                "-MinSDK_14-TargetSDK_19-N_", nrow(androzoo_data),
                ".csv", sep=''),
          quote=F)

# Step 3
print("Performing proportion test considering specific API levels")
target_sdks <- c(19, 21, 22, 23, 24, 25)
tmp1 <- mclapply(target_sdks, function(target_sdk) {
  data <- androzoo_data[androzoo_data$TargetSDK >= target_sdk &
                        androzoo_data$MinSDK <= target_sdk,]
  data_nrow <- nrow(data)
  num_of_samples <- get_num_of_samples(data_nrow)
  print(paste(date(), "Number of Samples",  num_of_samples,
              "Sample Size", SAMPLE_SIZE, "Target SDK", target_sdk,
              "Population Size", data_nrow))
  tmp1 <- t(as.data.frame(sapply(api_cols, function (api_col) {
    do_prop_test(api_col, data)
  })))
  rownames(tmp1) <- colnames(ghera_data)[api_cols]
  write.csv(tmp1,
            paste(prefix, "-n_", SAMPLE_SIZE, "-samples_",
                  num_of_samples, "-API_", target_sdk, "-N_", nrow(data),
                  ".csv", sep=''),
            quote=F)
}, mc.cleanup=T)
