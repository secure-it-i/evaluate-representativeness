# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause "New" and "Revised" License
#
# Author: Venkatesh-Prasad Ranganath


library(readr)
library(ggplot2)
library(ggpubr)

print("Loading data")
valid_min_sdks <- as.character(14:27)
valid_target_sdks <- as.character(19:27)
trim_based_on_minSDK_and_targetSDK <- function (data, pos) {
  subset(data, MinSDK %in% valid_min_sdks & TargetSDK %in% valid_target_sdks)
}

getPlot <- function(target_profile_file, api_ids_file=NULL) {
  target_data <- readr::read_csv_chunked(target_profile_file,
    readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
    progress=T)

  if (!is.null(api_ids_file)) {
    api_ids <- read.csv(api_ids_file, header=F, sep="!")
    tmp1 <- c(colnames(target_data)[1:3], as.character(api_ids$V1))
    target_data <- target_data[tmp1]
  }
  
  target_data <- target_data[!is.na(target_data$MinSDK) &
                                   !is.na(target_data$TargetSDK) &
                                   !is.na(target_data$APKs),]
  androzoo_data_nrow <- nrow(target_data)
  print(paste("Considering", nrow(target_data), "APKs"))
  
  print("Plotting API usage graph")
  target_sdks <- c(19, 21, 22, 23, 24, 25)
  
  androzoo_api_proportions <- sapply(target_sdks, function(target_sdk) {
    data <- target_data[target_data$TargetSDK >= target_sdk &
                            target_data$MinSDK <= target_sdk,]
    as.vector(apply(data[, -1:-3], 2, sum) / nrow(data) * 100)
  }, simplify=F, USE.NAMES=T)
  names(androzoo_api_proportions) <- target_sdks
  androzoo_api_proportions$all <- as.vector(
    apply(target_data[, -1:-3], 2, sum) / androzoo_data_nrow * 100)
  
  ord <- order(androzoo_api_proportions$`25`, decreasing=T)

  ggplot(NULL, aes(1:length(ord))) +
    geom_line(aes(y = androzoo_api_proportions$`19`[ord], colour = "19")) +
    # geom_line(aes(y = androzoo_api_proportions$`21`[ord], colour = "21")) +
    geom_line(aes(y = androzoo_api_proportions$`22`[ord], colour = "22")) +
    geom_line(aes(y = androzoo_api_proportions$`23`[ord], colour = "23")) +
    # geom_line(aes(y = androzoo_api_proportions$`24`[ord], colour = "24")) +
    geom_line(aes(y = androzoo_api_proportions$`25`[ord], colour = "25")) +
    labs(color="API Levels")
}

p_real_all <- getPlot("output-androzoo/real-world-benign-api-profile.csv")
p_real_all <- p_real_all + theme(axis.title.x=element_blank(),
                                 axis.title.y=element_text(size=14),
                                 axis.text.x=element_blank(),
                                 axis.text.y=element_text(size=12),
                                 axis.ticks.x=element_blank()) +
  ylab("Percentage of Apps Using an API")
  
p_real_sec <- getPlot("output-androzoo/real-world-benign-api-profile.csv", "output-androzoo/security/ghera-benign-id-api-mapping.csv")
p_real_sec <- p_real_sec + theme(axis.title.x=element_blank(),
                                 axis.title.y=element_blank(),
                                 axis.text.x=element_blank(),
                                 axis.text.y=element_blank(),
                                 axis.ticks=element_blank())

p_top200_all <- getPlot("output-top-200/real-world-benign-api-profile.csv")
p_top200_all <- p_top200_all + theme(legend.title=element_text(size=14),
                                     legend.text=element_text(size=14),
                                     axis.title.x=element_text(size=14),
                                     axis.title.y=element_text(size=14),
                                     axis.text.x=element_text(size=12),
                                     axis.text.y=element_text(size=12)) +
  xlab("APIs in decreasing order of proportions in API Level 25") +
  ylab("Percentage of Apps Using an API")
  
p_top200_sec <- getPlot("output-top-200/real-world-benign-api-profile.csv", "output-top-200/security/ghera-benign-id-api-mapping.csv")
p_top200_sec <- p_top200_sec + theme(axis.title.x=element_text(size=14),
                                     axis.title.y=element_blank(),
                                     axis.text.x=element_text(size=12),
                                     axis.text.y=element_blank(),
                                     axis.ticks.y=element_blank()) +
  xlab("APIs in decreasing order of proportions in API Level 25")

png(paste("api-usage-benign.png", sep=""), width=1600, height=800)
ggarrange(p_real_all, p_real_sec, p_top200_all, p_top200_sec, ncol=2, nrow=2, common.legend=TRUE, legend="bottom")
dev.off()

