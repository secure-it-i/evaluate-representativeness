/*
 * Copyright (c) 2018, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Authors: Venkatesh-Prasad Ranganath
 */

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

def cli = new CliBuilder(usage:"groovy generateSHA.groovy")
cli.a(longOpt:'androidHome', args:1, argName:'androidHome', required:true,
        'Absolute path to Android sdk folder')
cli.o(longOpt:'outputFile', args:1, argName:'outputFile', required:true,
        'File to store the SHA info')
def options = cli.parse(args)
if (!options) {
    return
}

def androidHome = options.a

List<String> slurpAttributeValue(slurper, String attribute, List<String> elements) {
    def tmp1 = "@$attribute".toString()
    slurper.'**'.findAll { it.name() in elements && !it[tmp1].isEmpty() }
            .collect { "A,${it.name()},$attribute=${it[tmp1].text()}" }
}

new File(options.o).withPrintWriter { def writer ->
    writer.println("apk,package,version code,version name,target API,min API,sha256") 
    def files = []
    new File('.').eachFileMatch { it.endsWith(".apk") } { files << it }
    files.parallelStream().map { File file ->
        println "Processing $file"
        def out = new StringBuilder()
        def err = new StringBuilder()
        def process = "$androidHome/tools/bin/apkanalyzer manifest print $file".execute()
        process.waitForProcessOutput(out, err)

        if (err.size() > 0) {
            println "Error while retrieving manifest from $file"
            println err
        } else if (out.size() > 0) {
            try {
                final pathResult = new XmlSlurper(false, true).parseText(out.readLines().join(''))
                final ret = [file.name]
                ret << pathResult.'@android:package'[0].text()
                ret << pathResult.'@android:versionCode'[0].text()
                ret << pathResult.'@android:versionName'[0].text()
                final sdks = pathResult.'**'
                    .findAll { it.name() == 'uses-sdk' }
                    .collect {
                        def minSdk = it.'@android:minSdkVersion'[0].text()
                        def tmp2 = it.'@android:targetSdkVersion'
                        def targetSdk = tmp2.isEmpty() ? minSdk : tmp2[0].text()
                        [targetSdk, minSdk]
                    }.unique().sort { -(it[0] as int) }
                ret.addAll(sdks.isEmpty() ? ['-1', '-1'] : sdks[0])  // -1 indicates default SDK version was injected
                final digest = MessageDigest.getInstance("SHA-256")
                ret << DatatypeConverter.printHexBinary(digest.digest(file.getBytes())).toLowerCase()
                return Optional.of(ret.join(','))
            } catch (Exception e) {
                println "Exception $e while processing manifest of $file ${e.message}"
                e.printStackTrace(System.out)
                return Optional.empty()
            }
        } else {
            println "Error: No output was generated for $file"
            return Optional.empty()
        }
    }.filter { it.isPresent() }
    .forEach {
        synchronized(writer) {
            writer.println(it.get())
        }
    }
}
