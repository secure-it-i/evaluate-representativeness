#!/usr/bin/env bash

# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Authors: Venkatesh-Prasad Ranganath

INPUT_FOLDER=input-may2018

exec &> >(tee output-may2018/graph-representativeness-log-pdf.txt)

echo "$(date) Plotting benign apps based representativeness"
Rscript scripts/plot_combined_api_usage.r benign benign 
mv combined-api-usage-benign.pdf output-may2018/

echo "$(date) Plotting malicious apps based representativeness"
Rscript scripts/plot_combined_api_usage.r malicious mal
mv combined-api-usage-malicious.pdf output-may2018/

echo "$(date) Plotting secure apps based representativeness"
Rscript scripts/plot_combined_api_usage.r secure secure
mv combined-api-usage-secure.pdf output-may2018/

echo "$(date) Plotting benign apps based representativeness limited to APIs common flagged and unflagged apps"
Rscript scripts/plot_combined_api_usage.r benign benign \
  $INPUT_FOLDER/androzoo/common-ghera-benign-id-api-mapping.csv \
  $INPUT_FOLDER/top-200-GooglePlay-04182018/common-ghera-benign-id-api-mapping.csv \
  common
mv combined-api-usage-common-benign.pdf output-may2018/

echo "$(date) Plotting benign apps based representativeness limited to APIs unique to flagged apps "
Rscript scripts/plot_combined_api_usage.r benign benign \
  $INPUT_FOLDER/androzoo/detected-only-ghera-benign-id-api-mapping.csv \
  $INPUT_FOLDER/top-200-GooglePlay-04182018/detected-only-ghera-benign-id-api-mapping.csv \
  detected-only
mv combined-api-usage-detected-only-benign.pdf output-may2018/

echo "$(date) Plotting benign apps based representativeness limited to APIs unique to unflagged apps "
Rscript scripts/plot_combined_api_usage.r benign benign \
  $INPUT_FOLDER/androzoo/undetected-only-ghera-benign-id-api-mapping.csv \
  $INPUT_FOLDER/top-200-GooglePlay-04182018/undetected-only-ghera-benign-id-api-mapping.csv \
  undetected-only
mv combined-api-usage-undetected-only-benign.pdf output-may2018/

echo "$(date) Plotting benign apps based representativeness limited to APIs in flagged apps "
Rscript scripts/plot_combined_api_usage.r benign benign \
  $INPUT_FOLDER/androzoo/detected-ghera-benign-id-api-mapping.csv \
  $INPUT_FOLDER/top-200-GooglePlay-04182018/detected-ghera-benign-id-api-mapping.csv detected
mv combined-api-usage-detected-benign.pdf output-may2018/

echo "$(date) Plotting benign apps based representativeness limited to APIs in unflagged apps "
Rscript scripts/plot_combined_api_usage.r benign benign \
  $INPUT_FOLDER/androzoo/undetected-ghera-benign-id-api-mapping.csv \
  $INPUT_FOLDER/top-200-GooglePlay-04182018/undetected-ghera-benign-id-api-mapping.csv undetected
mv combined-api-usage-undetected-benign.pdf output-may2018/
