# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
#   Authors: Joydeep Mitra
#
#  This script requires apk name as input
#	You can run this script in parallel : $ cat /path/to/sha/ | parallel partition.sh {}
#

targetSdk=`$ANDROID_HOME/tools/bin/apkanalyzer manifest target-sdk input/androzoo/Apks/$1.apk | bc`
minSdk=`~$ANDROID_HOME/tools/bin/apkanalyzer manifest min-sdk input/androzoo/Apks/$1.apk | bc`

if [[ $targetSdk -gt 18 ]] && [[ $minSdk -gt 13 ]]; then
	echo "moving $1"
	mv /mnt/Data1/AndroZoo-apks-150k/$1.apk /mnt/Data1/AndroZoo-apks-14-19/
fi
