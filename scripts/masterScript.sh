#!/usr/bin/env bash

# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Authors: Venkatesh-Prasad Ranganath
#          Joydeep Mitra

#REAL_WORLD_APKS=input/androzoo
REAL_WORLD_APKS=input/top-200-GooglePlay-04182018

#OUTPUT_FOLDER=output-androzoo
OUTPUT_FOLDER=output-top-200-GooglePlay-04182018

#ANDROID_HOME=~/Android/sdk
ANDROID_HOME=~/Library/Android/sdk

echo ""
echo "You will use the following settings:"
echo "  ANDROID_HOME=$ANDROID_HOME"
echo "  OUTPUT_FOLDER=$OUTPUT_FOLDER"
echo "  REAL_WORLD_APKS=$REAL_WORLD_APKS"
read -p "Are these settings correct? (yn) " answer

if [[ $answer != "y" ]] ; then
    echo "Change your setting in scripts/masterRun.sh and rerun it."
    exit
fi
echo ""

mkdir -p $OUTPUT_FOLDER/security

BASELINE_APKS=input/baseline/
BASELINE_ID_API_MAPPING=$OUTPUT_FOLDER/baseline-id-api-mapping.csv
BASELINE_API_PROFILE=$OUTPUT_FOLDER/baseline-api-profile.txt
BASELINE_API_PROFILE_CSV=$OUTPUT_FOLDER/baseline-api-profile.csv
BASELINE_API_PROFILE_LOG=$OUTPUT_FOLDER/baseline-api-profile.log

APIS_TO_IGNORE=$OUTPUT_FOLDER/apis-to-ignore.txt

GHERA_BENIGN_APKS=input/ghera/benign/
GHERA_BENIGN_ID_API_MAPPING=$OUTPUT_FOLDER/ghera-benign-id-api-mapping.csv
GHERA_BENIGN_API_PROFILE=$OUTPUT_FOLDER/ghera-benign-api-profile.txt
GHERA_BENIGN_API_PROFILE_CSV=$OUTPUT_FOLDER/ghera-benign-api-profile.csv
GHERA_BENIGN_API_PROFILE_LOG=$OUTPUT_FOLDER/ghera-benign-api-profile.log

GHERA_MALICIOUS_APKS=input/ghera/malicious/
GHERA_MALICIOUS_ID_API_MAPPING=$OUTPUT_FOLDER/ghera-mal-id-api-mapping.csv
GHERA_MALICIOUS_API_PROFILE=$OUTPUT_FOLDER/ghera-mal-api-profile.txt
GHERA_MALICIOUS_API_PROFILE_CSV=$OUTPUT_FOLDER/ghera-mal-api-profile.csv
GHERA_MALICIOUS_API_PROFILE_LOG=$OUTPUT_FOLDER/ghera-mal-api-profile.log

GHERA_SECURE_APKS=input/ghera/secure/
GHERA_SECURE_ID_API_MAPPING=$OUTPUT_FOLDER/ghera-secure-id-api-mapping.csv
GHERA_SECURE_API_PROFILE=$OUTPUT_FOLDER/ghera-secure-api-profile.txt
GHERA_SECURE_API_PROFILE_CSV=$OUTPUT_FOLDER/ghera-secure-api-profile.csv
GHERA_SECURE_API_PROFILE_LOG=$OUTPUT_FOLDER/ghera-secure-api-profile.log

GHERA_SECURITY_APIS=input/ghera-security-apis.txt
GHERA_SECURITY_BENIGN_ID_API_MAPPING=$OUTPUT_FOLDER/security/ghera-benign-id-api-mapping.csv
GHERA_SECURITY_MALICIOUS_ID_API_MAPPING=$OUTPUT_FOLDER/security/ghera-mal-id-api-mapping.csv
GHERA_SECURITY_SECURE_ID_API_MAPPING=$OUTPUT_FOLDER/security/ghera-secure-id-api-mapping.csv

REAL_WORLD_ID_API_MAPPING=$OUTPUT_FOLDER/real-world-id-api-mapping.csv
REAL_WORLD_API_PROFILE=$OUTPUT_FOLDER/real-world-api-profile.txt
REAL_WORLD_API_PROFILE_LOG=$OUTPUT_FOLDER/real-world-api-profile.log
REAL_WORLD_BENIGN_API_PROFILE_CSV=$OUTPUT_FOLDER/real-world-benign-api-profile.csv
REAL_WORLD_MALICIOUS_API_PROFILE_CSV=$OUTPUT_FOLDER/real-world-malicious-api-profile.csv
REAL_WORLD_SECURE_API_PROFILE_CSV=$OUTPUT_FOLDER/real-world-secure-api-profile.csv

RELEVANCE_ALL_CSV=$OUTPUT_FOLDER/relevance-summary.csv
RELEVANCE_SECURITY_CSV=$OUTPUT_FOLDER/security/relevance-summary.csv


echo "$(date) Collecting API profile for baseline APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $BASELINE_APKS -i $BASELINE_ID_API_MAPPING \
    -p $BASELINE_API_PROFILE > $BASELINE_API_PROFILE_LOG
cat $BASELINE_ID_API_MAPPING | cut -f2 -d'!' > $APIS_TO_IGNORE

echo ""
read -p "Do you want to use input/apis-to-ignore.txt? (yn) " answer

if [[ $answer = "y"  ]] ; then
    cp input/apis-to-ignore.txt $APIS_TO_IGNORE
else
    read -p "Edit $APIS_TO_IGNORE and then press any key."
fi
echo ""


#################################################################
#
# Ghera section
#
echo "$(date) Collecting API profile for Ghera Benign APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $GHERA_BENIGN_APKS \
    -i $GHERA_BENIGN_ID_API_MAPPING -p $GHERA_BENIGN_API_PROFILE \
    &> $GHERA_BENIGN_API_PROFILE_LOG

echo "$(date) Converting Ghera Benign API profile into CSV"
groovy scripts/remapProfile.groovy -s $GHERA_BENIGN_ID_API_MAPPING \
    -t $GHERA_BENIGN_ID_API_MAPPING -i $GHERA_BENIGN_API_PROFILE \
    -o $GHERA_BENIGN_API_PROFILE_CSV -g $APIS_TO_IGNORE

echo "$(date) Collecting API profile for Ghera Malicious APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $GHERA_MALICIOUS_APKS \
    -i $GHERA_MALICIOUS_ID_API_MAPPING -p $GHERA_MALICIOUS_API_PROFILE \
    &> $GHERA_MALICIOUS_API_PROFILE_LOG

echo "$(date) Converting Ghera Malicious API profile into CSV"
groovy scripts/remapProfile.groovy -s $GHERA_MALICIOUS_ID_API_MAPPING \
    -t $GHERA_MALICIOUS_ID_API_MAPPING -i $GHERA_MALICIOUS_API_PROFILE \
    -o $GHERA_MALICIOUS_API_PROFILE_CSV -g $APIS_TO_IGNORE

echo "$(date) Collecting API profile for Ghera Secure APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $GHERA_SECURE_APKS \
    -i $GHERA_SECURE_ID_API_MAPPING -p $GHERA_SECURE_API_PROFILE \
    &> $GHERA_SECURE_API_PROFILE_LOG

echo "$(date) Converting Ghera Secure API profile into CSV"
groovy scripts/remapProfile.groovy -s $GHERA_SECURE_ID_API_MAPPING \
    -t $GHERA_SECURE_ID_API_MAPPING -i $GHERA_SECURE_API_PROFILE \
    -o $GHERA_SECURE_API_PROFILE_CSV -g $APIS_TO_IGNORE

#################################################################
#
# Real world section
#
echo "$(date) Collecting API Profile for real world APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $REAL_WORLD_APKS \
    -i $REAL_WORLD_ID_API_MAPPING -p $REAL_WORLD_API_PROFILE \
    &> $REAL_WORLD_API_PROFILE_LOG

echo "$(date) Remapping real world API profile into Ghera Benign API profile"
groovy scripts/remapProfile.groovy -s $REAL_WORLD_ID_API_MAPPING \
    -t $GHERA_BENIGN_ID_API_MAPPING -i $REAL_WORLD_API_PROFILE \
    -o $REAL_WORLD_BENIGN_API_PROFILE_CSV -g $APIS_TO_IGNORE

echo "$(date) Remapping real world API profile into Ghera Malicious API profile"
groovy scripts/remapProfile.groovy -s $REAL_WORLD_ID_API_MAPPING \
    -t $GHERA_MALICIOUS_ID_API_MAPPING -i $REAL_WORLD_API_PROFILE \
    -o $REAL_WORLD_MALICIOUS_API_PROFILE_CSV -g $APIS_TO_IGNORE

echo "$(date) Remapping real world API profile into Ghera Secure API profile"
groovy scripts/remapProfile.groovy -s $REAL_WORLD_ID_API_MAPPING \
    -t $GHERA_SECURE_ID_API_MAPPING -i $REAL_WORLD_API_PROFILE \
    -o $REAL_WORLD_SECURE_API_PROFILE_CSV -g $APIS_TO_IGNORE


#################################################################
#
# Calculation and tabulation of relevance based on all APIs
#
echo "$(date) Calculating relevance for benign apps"
Rscript ./scripts/calculate-relevance.r \
    $GHERA_BENIGN_API_PROFILE_CSV $REAL_WORLD_BENIGN_API_PROFILE_CSV \
    $OUTPUT_FOLDER/relevant_benign

echo "$(date) Calculating relevance for malicious apps"
Rscript ./scripts/calculate-relevance.r \
    $GHERA_MALICIOUS_API_PROFILE_CSV $REAL_WORLD_MALICIOUS_API_PROFILE_CSV \
    $OUTPUT_FOLDER/relevant_malicious

echo "$(date) Calculating relevance for secure apps"
Rscript ./scripts/calculate-relevance.r \
    $GHERA_SECURE_API_PROFILE_CSV $REAL_WORLD_SECURE_API_PROFILE_CSV \
    $OUTPUT_FOLDER/relevant_secure

echo "$(date) Tabulating relevance for all APIs"
Rscript ./scripts/tabulate-relevance.r $OUTPUT_FOLDER \
    'relevant_benign.*' 'relevant_malicious.*' 'relevant_secure.*' \
    > $RELEVANCE_ALL_CSV

#################################################################
#
# Plot all-api proportion graphs
#
echo "$(date) Plotting benign all-API proportions graph"
Rscript ./scripts/plot_api_usage.r \
    $GHERA_BENIGN_API_PROFILE_CSV $REAL_WORLD_BENIGN_API_PROFILE_CSV \
    benign $OUTPUT_FOLDER
 
echo "$(date) Plotting malicious all-API proportions graph"
Rscript ./scripts/plot_api_usage.r \
    $GHERA_MALICIOUS_API_PROFILE_CSV $REAL_WORLD_MALICIOUS_API_PROFILE_CSV \
    malicious $OUTPUT_FOLDER

echo "$(date) Plotting secure all-API proportions graph"
Rscript ./scripts/plot_api_usage.r \
    $GHERA_SECURE_API_PROFILE_CSV $REAL_WORLD_SECURE_API_PROFILE_CSV \
    secure $OUTPUT_FOLDER


##################################################################
#
# Security API specific tasks begins here
#
echo "$(date) Making id-mapping file for security related apis in benign apps"
scripts/project-security-id-mapping.sh -i $GHERA_SECURITY_APIS \
    -s $GHERA_BENIGN_ID_API_MAPPING -t $GHERA_SECURITY_BENIGN_ID_API_MAPPING

echo "$(date) Making id-mapping file for security related apis in secure apps"
scripts/project-security-id-mapping.sh -i $GHERA_SECURITY_APIS \
    -s $GHERA_SECURE_ID_API_MAPPING -t $GHERA_SECURITY_SECURE_ID_API_MAPPING

echo "$(date) Making id-mapping file for security related apis in malicious apps"
scripts/project-security-id-mapping.sh -i $GHERA_SECURITY_APIS \
    -s $GHERA_MALICIOUS_ID_API_MAPPING \
    -t $GHERA_SECURITY_MALICIOUS_ID_API_MAPPING

echo "$(date) Projecting relevance for security related APIS in benign apps"
for i in `ls -1 $OUTPUT_FOLDER/relevant_benign*` ; do
    echo "start projection for $i"
    outfile=${i//$OUTPUT_FOLDER/$OUTPUT_FOLDER\/security}
    scripts/project-security-apis.sh -s $GHERA_SECURITY_BENIGN_ID_API_MAPPING \
        -t $i -o $outfile
    echo "done projection for $i"
done

echo "$(date) Projecting relevance for security related APIS in secure apps"
for i in `ls -1 $OUTPUT_FOLDER/relevant_secure*` ; do
    echo "start projection for $i"
    outfile=${i//$OUTPUT_FOLDER/$OUTPUT_FOLDER\/security}
    scripts/project-security-apis.sh -s $GHERA_SECURITY_SECURE_ID_API_MAPPING \
        -t $i -o $outfile
    echo "done projection for $i"
done

echo "$(date) Projecting relevance for security related APIS in malicious apps"
for i in `ls -1 $OUTPUT_FOLDER/relevant_malicious*` ; do
    echo "start projection for $i"
    outfile=${i//$OUTPUT_FOLDER/$OUTPUT_FOLDER\/security}
    scripts/project-security-apis.sh \
        -s $GHERA_SECURITY_MALICIOUS_ID_API_MAPPING -t $i -o $outfile
    echo "done projection for $i"
done


##################################################################
#
# Tabulation of relevance based only on security APIs
#
echo "$(date) Tabulating relevance for security-related APIs"
Rscript ./scripts/tabulate-relevance.r $OUTPUT_FOLDER/security \
    'relevant_benign.*' 'relevant_malicious.*' \
    'relevant_secure.*' > $RELEVANCE_SECURITY_CSV


#################################################################
#
# Plot security-specific-api proportion graphs
#
echo "$(date) Plotting benign security-specific-API proportions graph"
Rscript ./scripts/plot_api_usage.r \
    $GHERA_BENIGN_API_PROFILE_CSV $REAL_WORLD_BENIGN_API_PROFILE_CSV \
    benign $OUTPUT_FOLDER/security $GHERA_SECURITY_BENIGN_ID_API_MAPPING

Rscript ./scripts/plot_api_usage.r \
    $GHERA_SECURE_API_PROFILE_CSV $REAL_WORLD_SECURE_API_PROFILE_CSV \
    secure $OUTPUT_FOLDER/security $GHERA_SECURITY_SECURE_ID_API_MAPPING
