/* Copyright (c) 2018, Kansas State University
*
*  BSD 3-clause License
*
*  Authors: Joydeep Mitra
*           Venkatesh-Prasad Ranganath
* This script requires that the latest.csv file be downloaded from https://androzoo.uni.lu/lists and placed in the input/androzoo folder of this porject
*/

new File("sha1.txt").withPrintWriter{ def writer ->
  shaCount = 0
  count_15 = 0;
  count_16 = 0;
  count_17 = 0;
  count_18 = 0;
  seen = [] as Set
  lines = new File("../input/androzoo/latest.csv").readLines()
  n = lines.size()
  while(count_15 + count_16 + count_17 + count_18 < 400000){
    i = new Random().nextInt(n-2)
    lineArr = lines[i+1].split(',')
    date = lineArr[3].split('-')
    if(date[0] == "2018"){
      p = new Random().nextDouble()
      if (p < 0.8 && !(lineArr[0] in seen)){
        count_18++;
        seen.add(lineArr[0])
        writer.println(lineArr[0])
      }
    }
    if(date[0] == "2017" && !(lineArr[0] in seen)){
      p = new Random().nextDouble()
      if (p < 0.4 && !(lineArr[0] in seen)){
        count_17++;
        seen.add(lineArr[0])
        writer.println(lineArr[0])
      }
    }
    if(date[0] == "2016" && !(lineArr[0] in seen)){
      p = new Random().nextDouble()
      if (p < 0.2 && !(lineArr[0] in seen)){
        count_16++;
        seen.add(lineArr[0])
        writer.println(lineArr[0])
      }
    }
    if(date[0] == "2015" && !(lineArr[0] in seen)){
      p = new Random().nextDouble()
      if (p < 0.1 && !(lineArr[0] in seen)){
        count_15++
        seen.add(lineArr[0])
        writer.println(lineArr[0])
      }
    }
    if (((count_15 + count_16 + count_17 + count_18 % 1000) == 0) &&
    (count_15 + count_16 + count_17 + count_18) > 0){
      println "$count_15 $count_16 $count_17 $count_18"
    }
  }
  println "2015 selected = " + count_15
  println "2016 selected = " + count_16
  println "2017 selected = " + count_17
  println "2018 selected = " + count_18
}
