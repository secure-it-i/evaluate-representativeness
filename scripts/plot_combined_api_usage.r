# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause "New" and "Revised" License
#
# Author: Venkatesh-Prasad Ranganath


library(docopt)
library(readr)
library(ggplot2)

args <- docopt(
  "Usage: plot_combined_api_usage.r <all_api_app_type> <security_api_app_type> [<androzoo_api_ids>] [<top200_api_ids>] [<suffix>]")

print("Loading data")
valid_min_sdks <- as.character(14:27)
valid_target_sdks <- as.character(19:27)
trim_based_on_minSDK_and_targetSDK <- function (data, pos) {
  subset(data, MinSDK %in% valid_min_sdks & TargetSDK %in% valid_target_sdks)
}

filter_ids <- function(data, api_ids_file) {
  if (!is.null(api_ids_file)) {
    api_ids <- as.character(read.csv(api_ids_file, header=F, sep="!")$V1)
    col_names <- colnames(data)
    tmp1 <- col_names[-1:-3] %in% api_ids 
    tmp2 <- c(col_names[1:3], col_names[-1:-3][tmp1])
    data[tmp2]
  } else {
    data
  }
}

get_data <- function(target_profile_file, api_ids_file1=NULL, 
                    api_ids_file2=NULL) {
  target_data <- readr::read_csv_chunked(target_profile_file,
                                         readr::DataFrameCallback$new(trim_based_on_minSDK_and_targetSDK),
                                         progress=T)
  
  target_data <- filter_ids(target_data, api_ids_file1)
  target_data <- filter_ids(target_data, api_ids_file2)

  target_data <- target_data[!is.na(target_data$MinSDK) &
                               !is.na(target_data$TargetSDK) &
                               !is.na(target_data$APKs),]
  androzoo_data_nrow <- nrow(target_data)
  print(paste("Considering", nrow(target_data), "APKs"))
  print(paste("Considering", ncol(target_data) - 3, "APIs"))
  
  target_sdks <- c(19, 21, 22, 23, 24, 25)
  
  androzoo_api_percentages <- sapply(target_sdks, function(target_sdk) {
    data <- target_data[target_data$TargetSDK >= target_sdk &
                          target_data$MinSDK <= target_sdk,]
    as.vector(apply(data[, -1:-3], 2, sum) / nrow(data) * 100)
  }, simplify=F, USE.NAMES=T)
  names(androzoo_api_percentages) <- target_sdks

  ord <- order(androzoo_api_percentages$`25`, decreasing=T)
  ret <- sapply(target_sdks, function(target_sdk) {
   androzoo_api_percentages[[as.character(target_sdk)]][ord] 
  }, simplify=F, USE.NAMES=T)
  names(ret) <- target_sdks
  ret <- as.data.frame(ret)
  ret$api_id <- 1:length(ord)
  return(ret) 
}

above_threshold <- function(data1, data2, data3, data4, col, threshold) {
  r1=c(sum(data1[, col] >= threshold),
       sum(data2[, col] >= threshold),
       sum(data3[, col] >= threshold),
       sum(data4[, col] >= threshold))
  r2=r1 / c(nrow(data1), nrow(data2), nrow(data3), nrow(data4))
  list(r1, r2)
}

#app_type <- rep(c("benign"), 2)
#app_type <- c("malicious", "mal")
#app_type <- rep(c("secure"), 2)
app_type <- c(args[['all_api_app_type']], args[['security_api_app_type']])
androzoo_api_ids_file <- args[['<androzoo_api_ids>']]
top200_api_ids_file <- args[['<top200_api_ids>']]

real_relevant_data <- get_data(paste("output-androzoo/real-world-", app_type[1], 
                               "-api-profile.csv", sep=""), 
                              androzoo_api_ids_file)
real_relevant_data$dataset <- "AndroZoo"
real_relevant_data$apis <- "Relevant APIs"
real_sec_data <- get_data(paste("output-androzoo/real-world-", app_type[1], 
                               "-api-profile.csv", sep=""), 
                         androzoo_api_ids_file, 
                         paste("output-androzoo/security/ghera-", app_type[2],
                               "-id-api-mapping.csv", sep=""))
real_sec_data$dataset <- "AndroZoo"
real_sec_data$apis <- "Security Related APIs"
top200_relevant_data <- get_data(paste("output-top-200/real-world-", app_type[1],
                                 "-api-profile.csv", sep=""), 
                                top200_api_ids_file)
top200_relevant_data$dataset <- "Top 200"
top200_relevant_data$apis <- "Relevant APIs"
top200_sec_data <- get_data(paste("output-top-200/real-world-", app_type[1], 
                                 "-api-profile.csv", sep=""),
                           top200_api_ids_file,
                           paste("output-top-200/security/ghera-", app_type[2],
                                 "-id-api-mapping.csv", sep=""))
top200_sec_data$dataset <- "Top 200"
top200_sec_data$apis <- "Security Related APIs"

data <- rbind(real_relevant_data, real_sec_data, top200_relevant_data, top200_sec_data)
threshold <- 50
intercept=above_threshold(real_relevant_data, real_sec_data, 
                          top200_relevant_data, top200_sec_data, "X25",
                          threshold)
xintercept_data <- data.frame(
  intercept=intercept[[1]],
  dataset=c(rep(c("AndroZoo"), 2), rep(c("Top 200"), 2)),
  apis=rep(c("Relevant APIs", "Security Related APIs"), 2))

print(paste(c("intercept absolute:", intercept[[1]]), collapse=" "))
print(paste(c("intercept percentage:", intercept[[2]]), collapse=" "))

suffix <- args[['<suffix>']]
suffix <- ifelse(length(suffix) == 0, "", paste(suffix, "-", sep=""))
ggplot(data=data, aes(x=api_id)) + 
  geom_line(aes(y=X19, color="19")) +
  # geom_line(aes(y=X21, color="21")) + 
  geom_line(aes(y=X22, color="22")) +
  geom_line(aes(y=X23, color="23")) +
  # geom_line(aes(y=X24, color="24")) + 
  geom_line(aes(y=X25, color="25")) + 
  facet_grid(dataset ~ apis, scales="free_x") + 
  xlab("APIs in decreasing order of API use percentage in API level 25 specific sample") +
  ylab("Percentage of apps using an API") +
  labs(color="API levels") +
  theme(legend.title=element_text(size=12),
        legend.text=element_text(size=12),
        axis.title.x=element_text(size=12),
        axis.title.y=element_text(size=12),
        axis.text.x=element_text(size=10),
        axis.text.y=element_text(size=10),
        legend.position="bottom",
        strip.text=element_text(size=10)) +
  geom_vline(aes(xintercept=intercept), xintercept_data, color="red", linetype=2) +
  geom_hline(yintercept=threshold, color="red", linetype=2)
ggsave(paste("combined-api-usage-", suffix, app_type[1], ".pdf", sep=""), 
  width=8, height=5)

