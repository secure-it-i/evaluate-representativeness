This folder contains the following files:

1.  **ghera-security-apis.txt** lists the apis used in Ghera apps that are identified as related to security.
2.  **apis-to-ignore.txt** lists apis that should not be considered for relevance calculation.
3.  **androzoo/sha.txt** lists the sha256 of AndroZoo considered in our experiment.
4.  **top-200-GooglePlay-04182018/sha.txt** lists the sha256 of top 200 apps at Google Play store as of April 18, 2018.
